# ctfbox - A CTF VM by Wellington

## Tools included
* zsh
* [Pwndbg](https://github.com/zachriggle/pwndbg)
* QEMU static binaries
* [pwntools](https://github.com/Gallopsled/pwntools)
* Foremost
* tshark
* [ROPGadget](https://github.com/JonathanSalwan/ROPgadget)
* [binwalk](https://github.com/devttys0/binwalk)


## Vagrant

### Install VirtualBox
Check [Virtualbox](https://www.virtualbox.org/wiki/Downloads) for information on installing Virtualbox on your respective operating system.

### Install Vagrant
Check [VagrantUp](https://www.vagrantup.com/downloads.html) for information on installing vagrant.

### Fire up the VM
```
vagrant up
... Wait for installation
vagrant ssh
```

### Shared folder

`share` folder is mounted at `/home/vagrant/share` in the VM.
